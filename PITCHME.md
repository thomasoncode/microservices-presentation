# Microservices
 
---

## Small Monoliths

- Testable
- Deployable
- Scalable

---

## Large Monoliths

- Complexity
- Difficult Deployment
- Obsolete Technology Stack
- Scaling

--- 
 
## Microservices

- Scalable
- Maintainable 
- Deployable

---

## New Challenges

- Service definitiion
- Distrubted application
- Adoption

---

# Getiting Started

---

## Service Definition

- Group by business capability - what not how
- Domain Driven Design Subdomain - subset of the application
- Message driven
- Single Responsibility/Common Closure Princple

---

## Communication

|              | @css[text-bold](one-to-one)            | @css[text-bold](one-to-many)       |
|--------------|-----------------------|-------------------|
| @css[text-bold](synchronous)  | @css[fragment](request/response)     |                   |
| @css[text-bold](asynchronous) | @css[fragment](one-way notifications) | @css[fragment](publish/subscribe) |

--- 

## Testing





